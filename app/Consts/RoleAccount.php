<?php


namespace App\Consts;

abstract class RoleAccount
{
    const STUDENT = 1;
    const TEACHER = 2;
}