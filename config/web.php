<?php

return [
    'limit' => [
        'max'  => 100,
        'item' => 20
    ],
    'cdn_sc'          => [
        'url'  => 'http://127.0.0.1:8000',
        'path' => storage_path('tmp/sc'),
    ],
];
